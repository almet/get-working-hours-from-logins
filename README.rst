Compute working hours from logins and logouts
#############################################

I never know how much time I'm spending at work per week, so I wrote a little script that gets me this information out of logins / logouts.

It obviously only work if you have a separated computer for work and for your personal use. But that's my case!

Installation
============

It's pretty simple to install, here are a few lines for you::

    $ pip install login-workhours
    $ workhours
      Week #    Duration (hours)
    --------  ------------------
        19                3.66
        21                4.19
        23                0.18
        24                0.01
        25               63.96
        28               23.99
        29               23.96
        30                1.61
